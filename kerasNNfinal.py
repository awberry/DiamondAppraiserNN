#Will Frazier and Alex Berry
#keras implementation of a neural network

from keras.models import Sequential
from keras.layers import Dense
from keras.optimizers import SGD

import numpy as np
np.set_printoptions(threshold=np.nan)
#to print fullly verbose numpy arrays

import matplotlib.pyplot as plt

from diamond_data_reader import load_diamond_data

#random seed for reproducibility
np.random.seed(19)

#load data set
(X_train,Y_train,X_valid,Y_valid,X_test,Y_test) = load_diamond_data('diamond-data-processed.csv')

#run main function
def run_model():
    """
    Runs the neural network training and prediction functions, prompting
    the user for the number of epochs to train over and the size of the
    slice of the validation or test sets to use for predictions.
    """
    print("Do you want to predict using the validation set or the test set?")
    set_choice = input("Type v for validation or t for test: ")
    if set_choice == "v":
        type_set = 'v'
        print("Predict using the whole set or a slice?")
        set_size = input("Type w for the whole set or an integer for the slice size: ")
        if set_size == "w":
            size_pred = len(X_valid)
        if set_size.isdigit():
            size_pred = int(set_size)
    if set_choice == "t":
        type_set = 't'
        print("Predict using the whole set or a slice?")
        set_size = input("Type w for the whole set or an integer for the slice size: ")
        if set_size == "w":
            size_pred = len(X_test)
        if set_size.isdigit():
            size_pred = int(set_size)
    num_epochs = int(input("Enter the number of epochs to run: "))
    model = create_model()
    compile_model(model)
    history = fit_model(model, num_epochs)
    plot_loss(history)
    evaluate_model(model,type_set)
    predict_prices(model,size_pred,type_set)

#create model
def create_model():
    """
    Initializes the sequential neural network architecture with an input
    layer, 3 hidden layers, and output layer. The activation function is
    tanh except for the output layer which uses a linear function. The
    parameter weights are initilized with a random normal distribution.
    """
    model = Sequential()
    model.add(Dense(8, input_dim=8, 
                    kernel_initializer='random_normal',
                    bias_initializer='zeros',
                    activation='relu'))
    model.add(Dense(256, kernel_initializer='random_normal',
                    bias_initializer='zeros',
                    activation='relu'))
    model.add(Dense(124, kernel_initializer='random_normal',
                    bias_initializer='zeros',
                    activation='relu'))
    model.add(Dense(8, kernel_initializer='random_normal',
                    bias_initializer='zeros',
                    activation='relu'))
    model.add(Dense(1, kernel_initializer='random_normal',
                    bias_initializer='zeros',
                    activation='linear'))
    return model

def compile_model(model):
    """
    Compiles the neural network model using stochastic gradient descent
    as an optimizer and mean squared error as the loss function.
    """
    opt = SGD(lr=0.25)
    model.compile(loss='mse',
                     optimizer = opt,
                     metrics = ['mse'])

def fit_model(model,num_epochs):
    """
    Fits the model to the training set using batches over a given number
    of epochs. The batch is randomly chosen from the training set.
    """
    history = model.fit(X_train, Y_train, epochs=num_epochs, 
              batch_size=128, verbose=2,
              shuffle=True)
    return history

def plot_loss(model_history):
    """
    Plots the mean squared error as a function of the number of epochs.
    """
    plt.plot(model_history.history['loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.show()

def evaluate_model(model, type_set):
    """
    Prints the mean squared error using the validation sets.
    """
    if type_set == 'v':
        X_data_set = X_valid
        Y_data_set = Y_valid
    if type_set == 't':
         X_data_set = X_test
         Y_data_set = Y_test
    scores = model.evaluate(X_data_set, Y_data_set, batch_size = 128)
    if type_set == 'v':
        print(("\n%s on validation set: %.12f%%" % (model.metrics_names[1], scores[1]*100)))
    else:
        print(("\n%s on test set: %.12f%%" % (model.metrics_names[1], scores[1]*100)))

def price_differences(actual_prices,predictions,index_val,percent):
    """
    Finds the elementwise difference in prices between a slice of the predictions
    and a slice of the validation set, rounding to the nearest 2 decimal places. If
    the parameter 'percent' == True, will instead return the elementwise percent error
    between the slices.
    """
    size_predictions = len(predictions[:index_val])
    size_actual = len(actual_prices[:index_val])
    
    dn_predictions = np.reshape(denormalize(predictions[0:index_val]), (size_predictions, 1))
    dn_actual = np.reshape(denormalize(actual_prices[0:index_val]), (size_actual,1))
    
    raw_diff = np.reshape(np.squeeze(dn_predictions - dn_actual), (size_predictions,1))
    percent_diff = np.divide(raw_diff,dn_actual)
    if percent:
        return np.squeeze(np.around(percent_diff,2))*100
    else:
        return np.squeeze(np.around(raw_diff,2))

def predict_prices(model,size_pred,type_set):
    """
    Makes predictions using the fitted model on a given slice of the validation set
    or test set. Prints max and min predicted prices and actual prices as a comparison metric and
    prints elementwise differences as described in price_differences().
    """
    if type_set == 'v':
        X_data_set = X_valid
        Y_data_set = Y_valid
    if type_set == 't':
         X_data_set = X_test
         Y_data_set = Y_test
    predictions = np.squeeze(model.predict(X_data_set, batch_size=128, verbose=0))
    print("Max predicted price is: " + str(max(denormalize(predictions))))
    print("Max actual price is: " + str(max(denormalize(Y_data_set))))
    print("Min predicted price is: " + str(min(denormalize(predictions))))
    print("Min actual price is: " + str(min(denormalize(Y_data_set))))
    print("The first " + str(size_pred) + " predictions are: ")
    print(str(denormalize(predictions[:size_pred])))
    print("The first " + str(size_pred) + " actual prices are: ")
    print(str(denormalize(Y_data_set[:size_pred])))
    print("Some price differences by value are: ")
    print(str(price_differences(Y_data_set,predictions,size_pred,False)))
    print("Some price differences by percent are: ")
    print(price_differences(Y_data_set,predictions,size_pred,True))
    print("The average percent error was: " + str(np.mean(np.abs((price_differences(Y_data_set,predictions,size_pred,True))))))

def denormalize(value):
    """
    Denormalizes a real value or array of real values to be within the range
    326 to 18823.
    """
    return value*(18823-326)+326

