import csv

def load_wine_data():
    """
    Load the wine data from a csv file to data stream,
    then divide the data into training, validation, and
    test sets by 60%, 20%, and 20%, respectively.
    """
    data_stream = []
    with open('winequality-data.csv', 'r', encoding='utf-8') as csvfile:
        reader = csv.reader(csvfile, quotechar='|')
        next(reader)
        for row in reader:
            for entry in row:
                entry = float(entry)
            data_stream.append(row)
            #3919 entries total
    train_data = data_stream[0:2351] #60%
    valid_data = data_stream[2351:3527] #20%
    test_data = data_stream[3527:] #final 20%
    return (train_data, valid_data, test_data, data_stream)

"""
The rest of this file contains functions designed to
perform analytics on the data and guide development of
feature vector creation.
UPDATE: These functions are no longer necessary with the
new data set.
"""
#def fix_data_encoding(data_stream):
#    """
#    Fixes the unicode errors present in the data stream.
#    """
#    for row in data_stream:
#        for entry in row:
#            entry = ftfy.fix_encoding(entry)
#    fixed_stream = data_stream
#    outputFile = open("wine-data-130kfixed.csv", "w", newline='')
#    with outputFile:
#        writer = csv.writer(outputFile)
#        writer.writerows(fixed_stream)

#def count_types(data_stream):
#    """
#    Counts the different number of types of countries,
#    designations, provinces, region_1s, region_2s, varities,
#    and wineries.
#    """
#
#    country_list = []
#    designation_list = []
#    province_list = []
#    region_1_list = []
#    region_2_list = []
#    variety_list = []
#    winery_list = []
#    
#    for i in range(len(data_stream)):
#        #aggregating the distinct types into their lists
#        if data_stream[i][1] not in country_list:
#            country_list.append(data_stream[i][1])
#            #there are 49 countries
#        if data_stream[i][3] not in designation_list:
#            designation_list.append(data_stream[i][3])
#            #there are 30329 designations
##        if entry[5] not in province_list:
##            province_list.append(entry[5])
##        if entry[6] not in region_1_list:
##            region_1_list.append(entry[6])
##        if entry[7] not in region_2_list:
##            region_2_list.append(entry[7])
##        if entry[8] not in variety_list:
##            variety_list.append(entry[8])
##        if entry[9] not in winery_list:
##            winery_list.append(entry[9])
#
#    list_of_lists = []
#    list_of_lists.append(country_list)
#    list_of_lists.append(designation_list)
#    list_of_lists.append(province_list)
#    list_of_lists.append(region_1_list)
#    list_of_lists.append(region_2_list)
#    list_of_lists.append(variety_list)
#    list_of_lists.append(winery_list)
#    
#    #properly formatting the lists
#    for type_list in list_of_lists:
#        #sorting the lists
#        type_list.sort()
#        for entry in type_list:
#            for subentry in entry:
#                #striping each data point of whitespace
#                subentry = subentry.strip().strip("\'").strip("\"")
#                subentry = ftfy.fix_text_encoding(subentry)
        
    
    
#    print('The # of countries is: ' + str(len(country_list)))
#    print('And they are: ' + str(country_list))
#    print('The # of designations is: ' + str(len(designation_list)))
#    print('And they are: ' + str(designation_list))
#    print('The # of provinces is: ' + str(len(province_list)))
#    print('And they are: ' + str(province_list))


    
